# Kas Yocto KDE Images

[siemens/kas](https://github.com/siemens/kas) is a setup tool for bitbake based projects.  
It is usefull for declaratively maintaining multiple build configurations.

## Getting started
```
./kas-container build rpi4-langdale-qt6.4.2.yml 
```
* **kas-container** is a wrapper script for using kas with docker or podman
* **rpi4-langdale-qt6.4.2.yml** contains the configuration for a langdale & qt6.4.2 based build of kf6 for the raspberrypi4-64 machine

## Documentation
* [kas documentation](https://kas.readthedocs.io/en/latest/) 
* [Yocto KF6 project page](https://invent.kde.org/packaging/yocto-meta-kf6)
* [KDE Yocto chat](https://matrix.to/#/#yocto:kde.org)